import Vue from 'vue';
import Router from 'vue-router';

// in development-env not use lazy-loading, because lazy-loading too many pages will cause webpack hot update too slow. so only in production use lazy-loading

Vue.use(Router);

/* Layout */
import Layout from '../views/layout/Layout';

/**
 * hidden: true                   if `hidden:true` will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu, whatever its child routes length
 *                                if not set alwaysShow, only more than one route under the children
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noredirect           if `redirect:noredirect` will no redirct in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    title: 'title'               the name show in submenu and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar,
  }
 **/
export const constantRouterMap = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },
  {path: '/404', component: () => import('@/views/404'), hidden: true},
  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    name: 'Dashboard',
    hidden: true,
    children: [
      {path: 'dashboard', component: () => import('@/views/dashboard/index')}
    ]
  },
  {
    path: '/sign_data',
    component: Layout,
    redirect: 'noredirect',
    children: [
      {
        path: 'index',
        name: 'sign_data',
        component: () => import('@/views/sign_data/index'),
        meta: {title: '签到管理', icon: 'form'}
      }
    ]
  },
  {
    path: '/flow_data',
    component: Layout,
    redirect: 'noredirect',
    children: [
      {
        path: 'index',
        name: 'flow_data',
        component: () => import('@/views/flow_data/index'),
        meta: {title: '逐日平均流量表', icon: 'form'}
      }
    ]
  },
];

export default new Router({
  // mode: 'history',
  scrollBehavior: () => ({
    y: 0
  }),
  routes: constantRouterMap
});

export const asyncRouterMap = [
  // 404错误页面
  {
    path: '*',
    redirect: '/404',
    hidden: true
  }
];
