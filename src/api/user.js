import request from '@/utils/request'
import {
  GetVisitData
} from '@/utils';

// 加密方式登录
export function login(UserCode, UserPwd) {
  const loginData = GetVisitData({
    UserCode,
    UserPwd
  });
  return request({
    url: '/System/Login',
    method: 'post',
    data: loginData
  })
}

// 获取水文站上报数据列表
export function reportList(data) {
  const loginData = GetVisitData(data);
  return request({
    url: '/System/ReportList',
    method: 'post',
    data: loginData
  })
}

// 修改水文站上报信息
export function updateReport(data) {
  const loginData = GetVisitData(data);
  return request({
    url: '/System/UpdateReport',
    method: 'post',
    data: loginData
  })
}

// 导出Excel
export function exportExcel(data) {
  const loginData = GetVisitData(data);
  return request({
    url: '/System/ExportExcel',
    method: 'post',
    data: loginData
  })
}

// 获取逐日平均流量列表
export function flowList(data) {
  const loginData = GetVisitData(data);
  return request({
    url: '/System/FlowList',
    method: 'post',
    data: loginData
  })
}
// 导出逐日流量Excel
export function exportFlow(data) {
  const loginData = GetVisitData(data);
  return request({
    url: '/System/ExportFlow',
    method: 'post',
    data: loginData
  })
}


