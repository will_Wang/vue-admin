// import { login, logout, getInfo, addUser } from '@/api/user'
import {
  login
} from '@/api/user'
import {
  getToken,
  setToken,
  removeToken,
  removeUser,
  removePwd,
  setUser,
  setPwd,
  getUser,
  getPwd
} from '@/utils/auth'

const user = {
  state: {
    token: getToken(),
    id: null,
    name: '',
    avatar: '',
    roles: [],
    status: ''
  },

  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token
    },
    SET_ID: (state, id) => {
      state.id = id
    },
    SET_ROLES: (state, roles) => {
      state.roles = roles
    }
  },

  actions: {
    // 登录
    Login({
      commit
    }, userInfo) {
      const username = userInfo.username.trim();
      setUser(username);
      setPwd(userInfo.password);
      return new Promise((resolve, reject) => {
        login(username, userInfo.password).then(response => {
          // 加密返回结果
          commit('SET_ID', response.Data);
          setToken(response.Data);
          commit('SET_TOKEN', response.Data);
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 刷新重新获取信息
    GetInfo({
      commit,
      state
    }) {
      return new Promise((resolve, reject) => {
        const username = getUser();
        const pwd = getPwd();
        login(username, pwd).then(response => {
          commit('SET_ID', response.Data);
          setToken(response.Data);
          commit('SET_ROLES', 'admin');
          commit('SET_TOKEN', response.Data);
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 登出
    LogOut({
      commit,
      state
    }) {
      commit('SET_TOKEN', '');
      commit('SET_ROLES', []);
      removeToken();
      removeUser();
      removePwd();
    },

    // 前端 登出
    FedLogOut({
      commit
    }) {
      return new Promise(resolve => {
        commit('SET_TOKEN', '');
        removeToken();
        resolve()
      })
    }
  }
}

export default user
